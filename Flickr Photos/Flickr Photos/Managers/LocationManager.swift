//
//  LocationManager.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    
    static let sharedManager = LocationManager()
    
    fileprivate var locationManager:CLLocationManager!
    var currentLocation: CLLocation! {
        didSet {
            print("Current Location Updated!")
            NotificationCenter.default.post(name: Notification.Name(rawValue: kDidStartUpdatingLocationNotification), object: nil)
        }
    }
    
    var didStartUpdatingLocation: Bool!
    
    func startLocationManager() -> LocationManager? {
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        NSLog("Starting Location Manager Service.")
        
        if let _ = didStartUpdatingLocation {
            NSLog("Location Manager is already running.")
            return self
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }else{
            didStartUpdatingLocation = false
            return nil
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }else{
            didStartUpdatingLocation = false
            return nil
        }
        
        didStartUpdatingLocation = true
        return self
    }
    
    func stopLocationManager() {
        if self.didStartUpdatingLocation! {
            guard let locationManager = self.locationManager else {
                NSLog("Location Manager has not been initialized nothing to stop.")
                return
            }
            NSLog("Stopping location Manager.")
            locationManager.stopUpdatingLocation()
            self.locationManager = nil
            self.didStartUpdatingLocation = false
        }
    }
    
    fileprivate override init() {
        super.init()
        NSLog("Shared Mobile Service Location Manager initialized.")
        
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            guard let locationManager = self.locationManager else {
                return
            }
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.first!
        self.didStartUpdatingLocation = true
        self.stopLocationManager()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: kDidFailToUpdateLocationNotification), object: error)
    }
    
}
