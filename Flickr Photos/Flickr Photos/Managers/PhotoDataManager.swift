//
//  PhotoDataManager.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/29/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

class PhotoDataManager {
    
    static var photoSearchResult :FlickrPhotoSearchResponse?
    static var currentPhotoCount: Int!
    static var currentPage: Int = 1
}
