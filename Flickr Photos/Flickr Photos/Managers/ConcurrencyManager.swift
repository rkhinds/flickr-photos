//
//  ConcurrencyManager.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

class ConcurrencyManager {
    
    static let sharedManager = ConcurrencyManager()
    
    fileprivate var photoDownloadQueue: DispatchQueue!
    fileprivate var realmPersistanceQueue: DispatchQueue!
    fileprivate var UIUpdateQueue: DispatchQueue!
    
    var willPersistOnCompletion: Bool = false
    
    private init() {
        self.photoDownloadQueue = DispatchQueue(label: "com.flickrPhotos.photoDownloadQueue", qos: .userInitiated)
        self.realmPersistanceQueue = DispatchQueue(label: "com.flickrPhotos.realmPersistanceQueue", qos: .background)
        self.UIUpdateQueue = DispatchQueue.main
    }
    
    func executeAsync(block: @escaping (() -> ())) {
        photoDownloadQueue.async(execute: block)
    }
    
    func executeOnMain(block: @escaping (() -> ())) {
        UIUpdateQueue.async(execute: block)
    }
    
    func executeAsync(work: DispatchWorkItem, onCompletion: DispatchWorkItem, isPhotoDownload: Bool = true) {
        if isPhotoDownload {
            photoDownloadQueue.async {
                work.perform()
                work.notify(queue: self.UIUpdateQueue, execute: {
                    onCompletion.perform()
                })
            }
        }else{
            realmPersistanceQueue.async {
                work.perform()
                work.notify(queue: self.realmPersistanceQueue, execute: {
                    onCompletion.perform()
                })
            }
        }
    }
}

extension ConcurrencyManager {
    
    class func createWorkItem(block: @escaping (() -> Void), qos: DispatchQoS, flags: DispatchWorkItemFlags? = .enforceQoS) -> DispatchWorkItem {
        return DispatchWorkItem(qos: qos, flags: flags!, block: block)
    }
}
