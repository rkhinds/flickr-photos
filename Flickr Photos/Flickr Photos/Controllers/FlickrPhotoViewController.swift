//
//  FlickrPhotoViewController.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class FlickrPhotoViewController: UIViewController {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var rootBlurView: UIVisualEffectView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var ownerInfoView: UIView!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var ownerLocationLabel: UILabel!
    
    @IBOutlet weak var photoCountLabel: UILabel!
    
    fileprivate var flickrAPI: FlickrAPI!
    fileprivate var fetchedPhotos: [FetchedPhoto]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.flickrAPI = FlickrAPI.shared
        self.fetchedPhotos = []
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(FlickrPhotoViewController.searchForFlickrPhoto), name: NSNotification.Name(rawValue: kDidStartUpdatingLocationNotification), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FlickrPhotoViewController {
    
    fileprivate func prefetch(indexPaths: [IndexPath], completion: @escaping ((FetchedPhoto) -> ())) {
        for index in indexPaths {
            if (PhotoDataManager.photoSearchResult?.photo.count)! > index.row {
                let photo = PhotoDataManager.photoSearchResult?.photo[index.row]
                
                ConcurrencyManager.sharedManager.executeAsync {
                    self.flickrAPI.fetchFlickrPhoto((photo?.getStaticImageLink())!, success: {
                        data, error in
                        if error == nil {
                            guard let imgData = data else {
                                return
                            }
                            let request = FlickrPhotoRequest(method: FlickrAPIConstants.PhotoGetInfoMethod, photoId: (photo?.id)!)
                                self.flickrAPI.getPhotoInfoResult(request, success: {
                                    photoInfo, photoInfoError in
                                    if photoInfoError == nil {
                                        let fetchedPhoto = FetchedPhoto(data: imgData, photoInfo: photoInfo, photo: photo!)
                                        self.fetchedPhotos.append(fetchedPhoto)
                                        ConcurrencyManager.sharedManager.executeOnMain {
                                            completion(fetchedPhoto)
                                        }
                                    }
                                    }, failure: {
                                        photoInfoError in
                                        NSLog("Prefetch failed at index: \(index.row). Error: \(photoInfoError?.localizedDescription)")
                                })
                        }
                        }, failure: {
                            error in
                            NSLog("Prefetch failed at index: \(index.row). Error: \(error?.localizedDescription)")
                    })
                }
                
            }
            
        }
    }
    
}

extension FlickrPhotoViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = PhotoDataManager.photoSearchResult?.photo.count else {
            return 0
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kPhotoViewCell, for: indexPath) as! PhotoCollectionViewCell
        
        if fetchedPhotos.count > indexPath.row {
            let photo = fetchedPhotos[indexPath.row]
            let image = UIImage(data: photo.data)
            let title = photo.photo.title
            let description = photo.photoInfo.dateuploaded
            
            cell.imageView.image = image
            cell.photoTitleLabel.text = title
            cell.photoDescriptionLabel.text = description
            
            self.ownerNameLabel.text = photo.photoInfo.owner.userName
            self.ownerLocationLabel.text = "Coming soon."
            
           
        }else{
            self.prefetch(indexPaths: [indexPath], completion: {
                fetchedPhoto in
                let photo = fetchedPhoto
                
                let image = UIImage(data: photo.data)
                let title = photo.photo.title
                let description = photo.photoInfo.dateuploaded
                
                cell.imageView.image = image
                cell.photoTitleLabel.text = title
                cell.photoDescriptionLabel.text = description
                
                self.ownerNameLabel.text = photo.photoInfo.owner.userName
                self.ownerLocationLabel.text = "Coming soon."
            })
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = fetchedPhotos[indexPath.row]
        let zoomVC = FPStoryboard.sharedStoryboard.zoomableImageViewController()
        zoomVC.image = UIImage(data: photo.data)
        zoomVC.shotHeight = zoomVC.image.size.height
        zoomVC.shotWidth = zoomVC.image.size.width
        self.navigationController?.pushViewController(zoomVC, animated: true)
    }
    
}

extension FlickrPhotoViewController {
    
    func searchForFlickrPhoto() {
        let request = FlickrPhotoSearchRequest(useCurrentLocation: true)
        request.page = PhotoDataManager.currentPage
        ConcurrencyManager.sharedManager.executeAsync {
            self.flickrAPI.getPhotoSearchResult(request, success: {
                photoSearchResponse, error in
                if error == nil {
                    PhotoDataManager.photoSearchResult = photoSearchResponse
                    if let count = PhotoDataManager.photoSearchResult?.pages {
                        self.photoCountLabel.text = "Page \(PhotoDataManager.currentPage) of \(count)"
                    }
                    
                    self.collectionView.reloadData()
                }else{
                    let alert = UIAlertController(title: "Photo Search", message: "Something when wrong while attempting to fetch search result. Error: \(error?.localizedDescription)", preferredStyle: .alert)
                    let dismiss = UIAlertAction(title: "Dismiss", style: .destructive, handler: {
                        _ in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(dismiss)
                    self.present(alert, animated: true, completion: nil)
                }
                }, failure: {
                    error in
                    let alert = UIAlertController(title: "Photo Search", message: "Something when wrong while attempting to fetch search result. Error: \(error?.localizedDescription)", preferredStyle: .alert)
                    let dismiss = UIAlertAction(title: "Dismiss", style: .destructive, handler: {
                        _ in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(dismiss)
                    self.present(alert, animated: true, completion: nil)
            })

        }
    }
}
