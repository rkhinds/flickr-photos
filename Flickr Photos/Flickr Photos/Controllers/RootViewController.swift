//
//  RootViewController.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit
import RZTransitions

class RootViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        RZTransitionsManager.shared().defaultPresentDismissAnimationController = RZZoomAlphaAnimationController()
        RZTransitionsManager.shared().defaultPushPopAnimationController = RZCardSlideAnimationController()
        delegate = RZTransitionsManager.shared()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
