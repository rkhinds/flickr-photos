//
//  ZoomableImageViewController.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class ZoomableImageViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var scrollView: UIScrollView!
    var doubleTap: UITapGestureRecognizer!
    var image: UIImage!
    
    var shotWidth: CGFloat!
    var shotHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        self.scrollView = UIScrollView(frame: frame)
        
        self.scrollView!.bounces = false
        self.scrollView!.showsHorizontalScrollIndicator = false
        self.scrollView!.showsVerticalScrollIndicator = false
        self.scrollView!.isUserInteractionEnabled = true
        self.scrollView!.delegate = self
        self.scrollView!.bouncesZoom = false
        self.scrollView!.scrollsToTop = false
        self.scrollView!.maximumZoomScale = 2.0
        self.scrollView!.minimumZoomScale = 1.0
        
        self.doubleTap = UITapGestureRecognizer(target: self, action: #selector(ZoomableImageViewController.zoom(tapGesture:)))
        self.doubleTap!.numberOfTapsRequired = 2
        self.doubleTap!.numberOfTouchesRequired = 1
        self.scrollView!.addGestureRecognizer(self.doubleTap!)
        
        // This was run when the animation is complete
        self.imageView!.removeFromSuperview()
        self.scrollView!.addSubview(self.imageView!)
        self.view.addSubview(self.scrollView!)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadImage() {
        self.imageView!.image = image
        self.scrollView!.contentSize = image.size
        
        let imgwidth = CGFloat(self.view.bounds.size.width / image.size.width)
        let imgheight = CGFloat(self.view.bounds.size.height / image.size.height)
        let minZoom: CGFloat = min(imgwidth, imgheight)
        
        NSLog("image size: %@ / %f", NSStringFromCGSize(image.size), minZoom)
        
        if (minZoom <= 1) {
            self.scrollView!.minimumZoomScale = minZoom
            self.scrollView!.setZoomScale(minZoom, animated: false)
            self.scrollView!.zoomScale = minZoom
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ZoomableImageViewController: UIScrollViewDelegate {
    
    func zoom(tapGesture: UITapGestureRecognizer) {
        if (self.scrollView!.zoomScale == self.scrollView!.minimumZoomScale) {
            let center = tapGesture.location(in: self.scrollView!)
            let size = self.imageView!.image!.size
            let zoomRect = CGRect(x: center.x, y: center.y, width: (size.width / 2), height: (size.height / 2))
            self.scrollView!.zoom(to: zoomRect, animated: true)
        } else {
            self.scrollView!.setZoomScale(self.scrollView!.minimumZoomScale, animated: true)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView!
    }
    
}
