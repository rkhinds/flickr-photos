//
//  FlickrRequest.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

class FlickrRequest {
    
    var method: String! 
    
    var api_key: String! = {
        return FlickrAPIConstants.API_KEY
    }()
    
    var format: String! = {
        return "json"
    }()
    
    init(method: String) {
        self.method = method
    }
    
}
