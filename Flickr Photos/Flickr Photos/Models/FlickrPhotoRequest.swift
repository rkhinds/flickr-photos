//
//  FlickrPhotoRequest.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

class FlickrPhotoRequest: FlickrRequest, Mappable {
    
    var photo_id: String!
    
    init(method: String, photoId: String) {
        super.init(method: method)
        self.photo_id = photoId
    }
    
    required init?(map: Map) {
        super.init(method: map.JSON["method"] as! String)
        
    }
    
    func mapping(map: Map) {
        api_key <- map["api_key"]
        method <- map["method"]
        format <- map["format"]
        photo_id <- map["photo_id"]
    }
}
