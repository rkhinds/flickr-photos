//
//  FetchedPhoto.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/29/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

struct FetchedPhoto {
    
    var data: Data!
    var photoInfo: FlickrPhotoInfo!
    var photo: FlickrPhoto!
    
}
