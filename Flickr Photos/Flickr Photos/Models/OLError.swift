//
//  OLError.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

class OLError {
    
    class func generateGenericError(domain: String, code: Int, localized: String) -> NSError {
        let userInfo = [
            NSLocalizedDescriptionKey : localized
        ]
        let error = NSError(domain: domain, code: code, userInfo: userInfo)
        return error
    }
    
}

//MARK:- Error Messages.

let kNilNetworkResponse = "Response Object is nil."
let kNilNetworkResponseData = "Response data is nil"
let kNilNetworkResponseResultData = "Response Result data is nil"
let kNilNetworkResponseResultDataFailedToUnbox = "Response Result data is nil. Failed to unbox result value."
let kNilNetworkRequestFailed = "Response Result data is nil"
