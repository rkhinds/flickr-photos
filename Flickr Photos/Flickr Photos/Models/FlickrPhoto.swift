//
//  FlickrPhoto.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrPhoto: Mappable {
    
     var id: String!
     var owner: String!
     var secret: String!
     var server: String!
     var farm: NSNumber!
     var title: String!
     var isPublic: NSNumber!
     var isFriend: NSNumber!
     var isFamily: NSNumber!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        owner <- map["owner"]
        secret <- map["secret"]
        server <- map["server"]
        farm <- map["farm"]
        title <- map["title"]
        isPublic <- map["isPublic"]
        isFamily <- map["isFamily"]
        isFriend <- map["isFriend"]
    }

    func getStaticImageLink(_ size: FlickrAPIConstants.FlickrPhotoSize? = nil) -> String {
        guard let size = size else {
            return FlickrAPIConstants.getLinkToStaticPhoto("\(farm!)", serverId: "\(server!)", id: id!, secret: secret!, size: .Medium)
        }
        return FlickrAPIConstants.getLinkToStaticPhoto("\(farm!)", serverId: "\(server!)", id: id!, secret: secret!, size: size)
        
    }
}
