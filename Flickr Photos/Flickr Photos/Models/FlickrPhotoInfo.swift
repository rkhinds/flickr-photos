//
//  FlickrPhotoInfo.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrPhotoInfo: Mappable {
    

    var id: String!
    var secret: String!
    var server: String!
    var farm: NSNumber!
    var dateuploaded: String!
    var rotation: NSNumber!
    var originalsecret: String!
    var originalformat: String!
    var owner: Owner!
    var views: NSNumber!
    var media: String!

    var title: [String: String]!
    var visibility: [String: NSNumber]!
    var editability: [String: NSNumber]!
    var dates: Dates!
    var urls: URL!

    init() {}
    init?(map: Map) {

    }


    mutating func mapping(map: Map) {
        id <- map["id"]
        secret <- map["secret"]
        server <- map["server"]
        farm <- map["farm"]
        dateuploaded <- map["dateuploaded"]
        rotation <- map["rotation"]
        originalsecret <- map["originalsecret"]
        originalformat <- map["originalformat"]
        owner <- map["owner"]
        views <- map["views"]
        media <- map["media"]
        title <- map["title"]
        visibility <- map["visibility"]
        editability <- map["editability"]
        dates <- map["dates"]
        urls <- map["urls.url.0"]
    }


    struct Owner: Mappable {
        var nsid: String!
        var userName: String!
        var realName: String!
        var location: String!
        var iconServer: String!
        var iconFarm: Int!
        var path_alias: String!
        
        init?(map: Map) {
        
        }
        
        mutating func mapping(map: Map) {
            nsid <- map["nsid"]
            userName <- map["username"]
            realName <- map["realname"]
            location <- map["location"]
            iconServer <- map["iconserver"]
            iconFarm <- map["iconfarm"]
            path_alias <- map["path_alias"]
        }
    }

    struct Dates: Mappable {
        
        var posted: String!
        var taken: String!
        var takenGranularity: Int!
        var takenUnknown: Int!
        var lastUpdate: String!
        
        init?(map: Map) {
        
        }
        
        mutating func mapping(map: Map) {
            posted <- map["posted"]
            taken <- map["taken"]
            takenGranularity <- map["takengranularity"]
            takenUnknown <- map["takenunknown"]
            lastUpdate <- map["lastupdate"]
        }
    }
    
    struct URL: Mappable {
        var type: String!
        var _content: String!
        
        init() {}
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            type <- map["type"]
            _content <- map["_content"]
        }
    }
}
