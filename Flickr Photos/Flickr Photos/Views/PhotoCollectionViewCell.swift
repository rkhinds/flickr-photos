//
//  PhotoCollectionViewCell.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var photoInfoBlurView: UIVisualEffectView!
    @IBOutlet weak var photoTitleLabel: UILabel!
    @IBOutlet weak var photoDescriptionLabel: UILabel!
    
}
