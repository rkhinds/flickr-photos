//
//  Storyboard.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

/**
 The FPStoryboard provides a common simple API that gives access to the view controllers used in this project. 
 
 This is a singleton class so a reference request would look like the following
 
 ```
 let storyboardRef = FPStoryboard.sharedStoryboard
 ```
 
 */
class FPStoryboard {
    
    static let sharedStoryboard = FPStoryboard()
    
    private let storyboard: UIStoryboard
    
    private init () {
        self.storyboard = UIStoryboard(name: "Main", bundle: nil)
    }
    
    /**
     - Returns: The root view controller. In this case UINavigationController
     */
    func rootViewController() -> UINavigationController {
        let vc = storyboard.instantiateViewController(withIdentifier: kRootVC) as! UINavigationController
        return vc
    }
    
    /**
     - Returns: The flickr photo view controller as a FlickrPhotoViewController object.
     */
    func flickrPhotoViewController() -> FlickrPhotoViewController {
        let vc = storyboard.instantiateViewController(withIdentifier: kFlickrPhotoVCIdentifier) as! FlickrPhotoViewController
        return vc
    }
    
    /**
     - Returns: The photo zoom view controller as a ZoomableImageViewController object.
     */
    func zoomableImageViewController() -> ZoomableImageViewController {
        let vc = storyboard.instantiateViewController(withIdentifier: kZoomVCIdentifier) as! ZoomableImageViewController
        return vc
    }
}
