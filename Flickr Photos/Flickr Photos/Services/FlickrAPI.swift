//
//  FlickrAPI.swift
//  Flickr Photos
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper


class FlickrAPI {
    
    var className: String {
        return "FlickrAPI"
    }
    
    static let shared = FlickrAPI()
    
    private init() {}
    
    func getPhotoSearchResult(_ request: FlickrPhotoSearchRequest, success: ((FlickrPhotoSearchResponse?, NSError?) -> ())?, failure: ((NSError?) -> ())?) {
        let url = FlickrAPIConstants.BASE_URL
        let parameters = Mapper().toJSON(request)
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: nil).responseObject(keyPath: FlickrAPIConstants.RESPONSE_PHOTO_SEARCH_KEYPATH) {
            (response: DataResponse<FlickrPhotoSearchResponse>) in
            if self.logNetworkResponse(response: response, failure: failure) {
                let photoResponse =  self.transformFlickrResponseData(response.data!, keyPath: FlickrAPIConstants.RESPONSE_PHOTO_SEARCH_KEYPATH, type: FlickrPhotoSearchResponse())
                success?(photoResponse, nil)
            }else{
               failure?(OLError.generateGenericError(domain: self.className, code: 400, localized: kNilNetworkResponseResultData))
                return
            }
        }
    }
    
    func getPhotoGeoLocationResult(_ request: FlickrPhotoRequest, success: ((FlickrPhotoGeoLocationData, NSError?) -> ())?, failure: ((NSError?) -> ())?) {
        let url = FlickrAPIConstants.BASE_URL
        let parameters = Mapper().toJSON(request)
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: nil).responseObject(keyPath: FlickrAPIConstants.RESPONSE_GEOLOCATION_KEYPATH) {
            (response: DataResponse<FlickrPhotoGeoLocationData>) in
            if self.logNetworkResponse(response: response, failure: failure) {
                let geoResponse = self.transformFlickrResponseData(response.data!, keyPath: FlickrAPIConstants.RESPONSE_GEOLOCATION_KEYPATH, type: FlickrPhotoGeoLocationData())
                success?(geoResponse!, nil)
            }else{
                failure?(OLError.generateGenericError(domain: self.className, code: 400, localized: kNilNetworkResponseResultData))
                return
            }
        }
    }
    
    func getPhotoInfoResult(_ request: FlickrPhotoRequest, success: ((FlickrPhotoInfo, NSError?) -> ())?, failure: ((NSError?) -> ())?) {
        let url = FlickrAPIConstants.BASE_URL
        let parameters = Mapper().toJSON(request)
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: nil).responseObject(keyPath: FlickrAPIConstants.RESPONSE_GEOLOCATION_KEYPATH) {
            (response: DataResponse<FlickrPhotoInfo>) in
            if self.logNetworkResponse(response: response, failure: failure) {
                let photoInfoResponse = self.transformFlickrResponseData(response.data!, keyPath: FlickrAPIConstants.RESPONSE_GEOLOCATION_KEYPATH, type: FlickrPhotoInfo())
                success?(photoInfoResponse!, nil)
            }else{
                failure?(OLError.generateGenericError(domain: self.className, code: 400, localized: kNilNetworkResponseResultData))
                return
            }
        }
    }
    
    func fetchFlickrPhoto(_ url: String, success: ((Data?, NSError?) -> ())?, failure: ((NSError?) -> ())?)  {
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseData {
            (data: DataResponse<Data>) in
            if data.result.isSuccess {
                success?(data.result.value, nil)
                return
            }else{
                failure?(data.result.error as NSError?)
                return
            }
        }
    }
    
}

extension FlickrAPI {
    
    fileprivate func logNetworkResponse<T>(response: DataResponse<T>, failure: ((NSError?) -> Void)?) -> Bool {
        guard response.response != nil else {
            failure?(OLError.generateGenericError(domain: self.className, code: 400, localized: kNilNetworkResponse))
            return false
        }
        
        guard let data = response.data else {
            failure?(OLError.generateGenericError(domain: self.className, code: 400, localized: kNilNetworkResponseData))
            return false
        }

        let search = String(data: data, encoding: String.Encoding.utf8)
        let success = (search?.characters.count)! > 5
        NSLog("Response success: [ \(success) ]")
        
        return success
    }
    
    fileprivate func transformFlickrResponseData<T: Mappable>(_ data: Data, keyPath: String, type: T) -> T? {
        let str = String(data: data, encoding: String.Encoding.utf8)
        let jsonString = self.flickrPhotoSearchSubString(str!)
        let json = self.convertStringToDictionary(jsonString)
        let results = Mapper<T>().map(JSONObject: json![keyPath])
        guard let res = results else {
            return nil
        }
        return res
    }
    
    fileprivate func flickrPhotoSearchSubString(_ string: String) -> String {
        return string[string.characters.index(string.startIndex, offsetBy: 14)..<string.characters.index(string.endIndex, offsetBy: -1)]
    }
    
    fileprivate func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
}
